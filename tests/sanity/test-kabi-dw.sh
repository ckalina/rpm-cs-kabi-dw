#!/usr/bin/env sh

TESTS+=(test_kabidw)

DESCRIPTION_test_kabidw=(
	"Invoke kabi-dw test suite."
)

function test_kabidw()
{
        local stdout_log="$1"
        local stderr_log="$2"
        shift 2

	OLDCWD="$(pwd)"

	tar xf testsuite.tar
	cd testsuite

	make clean all KABI_DW="$KABI_DW_BIN" 2> $stderr_log | tee $stdout_log

	# Strip away paths
	sed -i 's/^File:.*$//' $(find . -path "*/output/*" -type f -iname "*.txt")

	comm -12 \
		<(find . -type d -iname "output" -exec dirname {} \; | sort | uniq) \
		<(find . -type d -iname "reference" -exec dirname {} \; | sort | uniq) \
	| xargs -I% bash -c '[[ $(grep -rh "^Version:" %/{output,reference}/ | sort | uniq | wc -l) != 1 ]] && { echo "[VERSION MISMATCH] Test Case: $(basename %)"; exit; } ; diff -qr %/{output,reference}/ || false'

	if test $? -gt 0
	then
                echo
		echo "$? ERROR: failed with non-zero code."

		echo "STDOUT {"
		cat $stdout_log
		echo "}"

		echo "STDERR {"
		cat $stderr_log
		echo "}"

		return 1
	fi

	cd "$CWD"

	return 0
}
