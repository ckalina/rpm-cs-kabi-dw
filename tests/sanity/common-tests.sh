if test -z "$MANUAL"
then
        export COLOR_NC=
        export COLOR_WHITE=
        export COLOR_BLACK=
        export COLOR_BLUE=
        export COLOR_LIGHT_BLUE=
        export COLOR_GREEN=
        export COLOR_LIGHT_GREEN=
        export COLOR_CYAN=
        export COLOR_LIGHT_CYAN=
        export COLOR_RED=
        export COLOR_LIGHT_RED=
        export COLOR_PURPLE=
        export COLOR_LIGHT_PURPLE=
        export COLOR_BROWN=
        export COLOR_YELLOW=
        export COLOR_GRAY=
        export COLOR_LIGHT_GRAY=
else
        export COLOR_NC='\e[0m'
        export COLOR_WHITE='\e[1;37m'
        export COLOR_BLACK='\e[0;30m'
        export COLOR_BLUE='\e[0;34m'
        export COLOR_LIGHT_BLUE='\e[1;34m'
        export COLOR_GREEN='\e[0;32m'
        export COLOR_LIGHT_GREEN='\e[1;32m'
        export COLOR_CYAN='\e[0;36m'
        export COLOR_LIGHT_CYAN='\e[1;36m'
        export COLOR_RED='\e[0;31m'
        export COLOR_LIGHT_RED='\e[1;31m'
        export COLOR_PURPLE='\e[0;35m'
        export COLOR_LIGHT_PURPLE='\e[1;35m'
        export COLOR_BROWN='\e[0;33m'
        export COLOR_YELLOW='\e[1;33m'
        export COLOR_GRAY='\e[0;30m'
        export COLOR_LIGHT_GRAY='\e[0;37m'
fi

function pass()
{
	if ! test -z "$MANUAL"
	then
		echo -en " $COLOR_GRAY$(printf "%0.s-" {1..35})"
		echo -en "[ ${COLOR_GREEN}PASS ]"
		echo -e "$COLOR_GRAY$(printf "%0.s-" {1..36})$COLOR_NC"
	else
		rlPass
	fi
}

function fail()
{
	if ! test -z "$MANUAL"
	then
		echo -en " $COLOR_GRAY$(printf "%0.s-" {1..35})"
		echo -en "[ ${COLOR_RED}FAIL ]"
		echo -e "$COLOR_GRAY$(printf "%0.s-" {1..36})$COLOR_NC"
	else
		rlFail
	fi
}

function print_test_case()
{
	if test -z "$MANUAL"
	then
		return 0
	fi
        echo
        local msg=" $COLOR_GRAY[${COLOR_BLUE}TEST $1$COLOR_GRAY]"
        eval msg="\$msg\$(printf "%0.s-" {1..$[80-${#msg}+${#COLOR_BLUE}+2*${#COLOR_GRAY}]})"
        echo -e "$msg$COLOR_NC"
}

function print_description()
{
        desc=()
        eval desc=\(\"\${DESCRIPTION_$1[@]}\"\)
        for desc_line in "${desc[@]}"
        do
                echo "       $desc_line"
        done
	echo
}

#
# Perform a test
#
# $1	test function
#
function run_test()
{
	rlPhaseStartTest "$1"

	if test $# -eq 0 -o -z $1
	then
		rlFail "ERROR: Expected test name to be passed to run_test" \
		       "function." >&2
		exit 1
	fi

        local stdout_log="$2"
        local stderr_log="$3"

	if test -z $stdout_log -o ! -e $stdout_log
	then
		rlFail "ERROR: Test inconsistency, STDOUT log file missing or" \
		       "non-existent." >&2
		exit 1
	fi

	if test -z $stderr_log -o ! -e $stderr_log
	then
		rlFail "ERROR: Test inconsistency, STDERR log file missing or" \
		       "non-existent." >&2
		exit 1
	fi

	echo > $stdout_log
	echo > $stderr_log

        print_test_case "$1"

        $1 "$stdout_log" "$stderr_log"
        ret=$?

        print_description "$1"

	if test $ret -eq 0
	then
		pass
	else
                fail

		echo "STDOUT {"
		cat $stdout_log
		echo "}"

		echo "STDERR {"
		cat $stderr_log
		echo "}"

		return 1
	fi

	rlPhaseEnd
}
